Change Log
###########

Development
-----------

7.0.0
-----

Added
^^^^^

 * Support for the ``AssignResources`` SDP command payload version 1.0.

Optimised
^^^^^^^^^

 * Improved performance of ``ChannelRange.end_id`` property and
   ``ChannelRange.contains_channel``, both of which are used during payload
   aggregation on the receiver's hot path.
   Performance increases by ~7% for observations with 10k channels.

Removed
^^^^^^^

 * Support for the ``AssignResources`` SDP command payload version 0.3.

6.5.2
-----

Added
^^^^^

 * Added support for SKART

6.5.1
-----

Changed
^^^^^^^

* OBSERVER and TELESCOPE_NAME are now set from the environment.
  this addresses bug SKB-590


6.5.0
-----

Added
^^^^^

* Added ICD timestamp utilities.

Changed
^^^^^^^

* ``MidICD`` epoch changed from Unix to the start of 2000 TAI.

6.4.0
-----

Added
^^^^^

* Added support specifying FLAG, WEIGHT, and SIGMA column values in MSWriter.
* New sizing utility methods in the ICD classes, useful to calculate packet/heap sizes.

Fixed
^^^^^

* ``POINTING`` tables created using ``MeasurementSet`` now contain correct reference frames - ``TARGET`` and ``DIRECTION`` will be ``AZEL``, while ``POINTING_OFFSET`` and ``SOURCE_OFFSET`` have no reference frame.

Changed
^^^^^^^

* Updated to poetry 1.8.2.

6.3.0
-----

Added
^^^^^

* ``msutils.ms_timestamp_rows()`` has been added which allows for iterating
  over the rows in the MS ``MAIN`` table for each timestamp.

Changed
^^^^^^^

* ``msutils.vis_reader()`` now verifies that the timestamps in the ``MAIN``
  table match it's expectations: that timestamps are grouped in ascending
  order, and there are the same number of rows per timestamp as there are
  baselines in the data. If this condition is not met, then a ``ValueError``
  will be raised.

6.2.0
-----

Added
^^^^^

* ``MSWriter.write_data_row()`` has been added which allows a full
  spectral window's worth of data to be written at once without having
  to specify any indexes or channel counts.

Fixed
^^^^^

* Handle a single channel in ``ChannelRange.from_ndarray()``,
  defaulting to a stride of 1.
* Spectral Windows with a stride greater than 1 will now be correctly
  detected from Measurement Sets.

Deprecated
^^^^^^^^^^

* ``MeasurementSet.channel_range`` actually returns the channel indexes.
  Use ``MeasurementSet.calculate_scan_type()`` instead and extract the
  spectral windows contained within.

6.1.0
-----

Added
^^^^^

* Added new UVW Engine based on the ERFA routines. It corrects for
  precession, nutation and polar motion. But does not correct for
  diurnal and annual aberration

Fixed
^^^^^

* Fixed ``Baselines()`` raising when constructed with antennas that
  correspond with baselines without autocorrelations.
* Fixed automatic maximum buffer size calculation
  for non-Linux POSIX platforms.

6.0.0
-----

Added
^^^^^

* Add new ``ChannelRange`` class to assist with more robust handling
  of channels starting at non-zero and non-contiguous values.

Changed
^^^^^^^

* **BREAKING** The ``SpectralWindow`` class now generates correct channel
  frequencies and bandwidths. This information is now used when reading
  and writing to a Measurement Set.
* **BREAKING** The ``SpectralWindow.freq_inc`` property has been renamed to
  ``SpectralWindow.channel_width``
* **BREAKING** ``ChannelRange.from_spectral_window()`` has been moved
  to ``SpectralWindow.as_channel_range()``
* **BREAKING** ``ChannelRange.try_from_spectral_window()`` has been moved
  to ``SpectralWindow.try_as_channel_range()``
* Optimise calls to ``write_pointings()`` by writing to columns
  in bulk and using ``TiledColumnStMan`` for columns that
  store pointing data.
* Change default value of ``Pointing.pointing_offset`` and
  ``Pointing.source_offset`` to be ``(0.0, 0.0)`` to simplify
  bulk writing logic and to align with the Measurement Set
  default value.
* Antenna class has been updated to accept the new Antenna Schema.
  An exception will be raised however if a new schema member (station_id)
  does not exist in the model. This is to prevent silent failures.
* Antenna methods have been added to recast the Antenna Class as
  a Katpoint.Antenna object
* Switch ``Pointing`` class to be a ``NamedTuple`` rather
  than a ``@dataclass`` to improve performance on construction.

Fixed
^^^^^

* ``POINTING`` table is now generated with default measurement
  set column definitions.

Removed
^^^^^^^

* Support for parsing INI files.


6.0.0-alpha.1
-------------

Changed
^^^^^^^

* Antenna class has been updated to accept the new Antenna Schema.
  An exception will be raised however if a new schema member (station_id)
  does not exist in the model. This is to prevent silent failures.
* Antenna methods have been added to recast the Antenna Class as
  a Katpoint.Antenna object
* Switch ``Pointing`` class to be a ``NamedTuple`` rather
  than a ``@dataclass`` to improve performance on construction.


5.3.0-alpha.1
-------------

Added
^^^^^

* Add new ``ChannelRange`` class to assist with more robust handling
  of channels starting at non-zero and non-contiguous values.

5.2.0
-----

Added
^^^^^

* New ``socket_utils`` module
  with functions to read the maximum
  read/write socket buffer sizes.


5.1.3
-----

Changed
^^^^^^^

* Fixed the value written into he ``TOTAL_BANDWIDTH`` column
  of the ``SPECTRAL_WINDOW`` Measurement Set subtable,
  it was previously recorded as a negative value.

5.1.2
-----

Added
^^^^^

* The Baseline class can now return the Baselines as a sequence of tuples.


5.1.1
-----

Added
^^^^^

* The UVWEngine can now take a pre-calculated baseline indices sequence. it
is also able to calculate a baseline order directly from the baseline utilities.
This allows non-standard baseline orders to be used.

5.0.1
-----

Fixed
^^^^^

* The baseline ordering given at ``MeasurementSet`` construction time
  was being ignored, being always overridden with a upper-triangular order.

Added
^^^^^

* New ``baseline_utils.baseline_indices`` utility method
  to centrally generate baseline indices with/without autocorrelation,
  and using different sequencing order.


Changed
^^^^^^^

* ``Baselines.generate`` gained new parameter to specify
  baseline sequencing order.


5.0.1
-----

Various small changes to add missing elements to the measurement sets - or fix existing ones:


Fixed
^^^^^

The REFERENCE and DELAY DIR cells were not being added to the FIELD table.
Currently the assumption is all three are the same.
Only PHASE_DIR is currently set in the configuration schema.
So we have to have a default here.

The MEAS_FREQ_REF this is a number from the MFrequency enumeration in CASACORE.
It represents the TOPOCENTRIC frame. This is the same frame the correlator is operating in.
Once again - there is no entry in the configuration for the FREQUENCY FRAME but TOPO is a good assumption.

SIGMA now defaults to [1,1,1,1] into the measurement set for each row.
It's a relative weight measure that is meant to represent the RMS of each correlation
in a single channel. We had defaulted to 0 - but as it is a weight some packages
multiply the visibilities by it ... resulting in bad things.
Resetting to 1 avoids this and is also done by ASKAP.

Added
^^^^^

Added a new class FrequencyType to make this enumeration clearer.
This follows casacore::MFrequency::Types.

5.0.0
-----

Fixed
^^^^^

* **BREAKING** Mark ``SPS_EPOCH`` on ``LowICD``
  as transmitted with the start-of-stream heap (rather than with
  data heaps) which is the correct behaviour as specified in the ICD.

Added
^^^^^

* Added the ability to write to the measurement set
  POINTING table
* New utility methods to LowICD for converting unix timestamps to Signal
  Processing System timestamps, epochs & offsets
* Add ``msutils.vis_mjd_epoch()`` which will return the earliest timestamp
  in the visibility data

Removed
^^^^^^^

* **BREAKING** Remove ``LowICD.unix_to_icd()``


4.0.0
-----

Added
^^^^^

* Added the ``cci`` member to the ``Payload`` class
  to hold CCI data sent by Mid.

Changed
^^^^^^^

* **BREAKING** Reorganised how ICD information is structured
  and presented to users:
  instead of a single ``Items`` enumeration
  and a few methods to convert time from-to the ICD format,
  there are now two top ``LowICD`` and ``MidICD`` classes,
  each containing all the details of their respective ICD.
  This includes the telescope they apply to,
  an ``Items`` enumeration,
  time conversion functions,
  and various pieces of convenience, pre-calculated information
  about the items.
* **BREAKING** Moved time conversion routines that are ICD-independent
  into new ``time_utils`` module.
* **BREAKING** ``msutils`` doesn't perform automatic ICD format conversions anymore;
  these are all left to the caller.
  When writing, all input data must be given
  in the format expected by the Measurement Set;
  conversely when reading all data is returned
  in the same format it had in the Measurement Set.


3.6.0
-----

New features
^^^^^^^^^^^^

* New ``autocast_fields`` class decorator for dataclasses
  that automatically changes the type of values
  given at construction time
  into the value the field is annotated with.

Improvements
^^^^^^^^^^^^

* Removed internal dependency on built-in distutils module,
  which is slated for removal in Python 3.12.
* Declared dependency of package on python >= 3.7.
  Previously there was no declaration
  of particular required python versions,
  but historically we have been
  implicitly bound to 3.7.


3.5.1
-----

* Bumped the katpoint version to 1.0a2 which is the current latest version


3.5.0
-----

* Removed astropy warning from UVWEngine
  due to invalid comparison of TestDelta against unit-less value.

* Added the KatpointUVWEngine as a derived UVWEngine class. It has the same
  interface so can be used as a drop in replacement for the measures based
  UVWEngine.

3.4.0
-----

* Changing to the new schema involves a restructuring of the Anntenna dataclass
  The read API for this class is the same. But the create API is now different
  hence the minor version bump

3.3.2
-----

* Antenna Utils load_layout is now aware of an alternate schema that uses
  'receptors' but does not validate the schema

3.3.1
-----

* Adjust scan classes and examples against latest ADR-54 schemas.
* Removed dependency on ska-sdp-dal.

3.3.0
-----

* Simplified GitLab CI infrastructure to use existing templates.
* Removed Docker image building and publishing
  since this repository's main product is a python package
  that is already published to the CAR.
* Moved code used only by receive-modules out from this repository
  and into the former.
* Removed unnecessary dependencies.

3.2.0
-----

Introduced the UVWEngine into the system as a machine to
calculate the UVW. This is now used in the interface
to realtime.receive.modules.consumers - hence the
minor version number bump.


3.1.1
-----
* Simplified BaseTM interface
* Multiple spectral window support for Measurement Sets

3.1.0
-----

* Made plasma dependencies required.

3.0.1
-----

* Added centralised definition of ICD heap Items
  for reuse in other code.

3.0.0
-----

* Added scan model
* Added configurable writing to SCAN_NUMBER in mswriter
* Using updated ska-sdp-dal-schemas

2.0.6
-----

* Removed a duplicate, incorrect way of calculating spectral windows.
* The SchedTM telescope model class now needs raises an exception if
  instantiated without an antenna layout.
* Added Measures to the Dockerfile

2.0.5
-----

Added the antenna to the sched_tm should be able to update the ANTENNA table in an MS

2.0.4
-----

Now tests for a strange occurance - perhaps even a race condition - that breaks the scheduling block logic

2.0.3
-----

Adding the ability to search a measurement set in memory for the correct row to insert data

2.0.3
-----

Core module exporting all classes and functions

2.0.2
-----

In the changes enforced by earlier linting steps I changed a comparision
opertor in an assert from == to is. This was a mistake. Switching it back. I
think we have full functionality now

2.0.1
-----

Changed to a namespaced package. Not changing the major version for this as it was pretty much unusable in the last state

2.0.0
-----

Major refactor into 3 repos. The core utils repo, the receive modules and the emulator. Hence the mahor version change. There is no change to functionality. This repository is the CORE repository it now contains the
interfaces and utility functions for both the receive modules and the emulator

No new dependencies have been introduced

Package name change realtime.receive.core


1.6.11
------

Reversed the logic in the handling of external models - so now scheduling block based models are prioritised


1.6.10
------

Adding the functionality to create a measurement set from the scheduling block. This replaces the need to have a data model. Essentially I have refactored the TM classes to inherit from a BaseTM abstract base class - there are now three:

The original FakeTM which takes a measurement set in construction
The PlasmaTM which uses the contents of the plasma store in construction
[new] SchedTM which uses a scheduling block instance.

The 'get_nearest_data" is overloaded in these cases to simply return an empty UVW vector. Which is added to the measurement set. This seems to work fine in all the test cases - but I have had to remove checking the 'value' of the UVW tables in the MSAsserter used in the unit tests - as there are no UVW.

* Added schedblock parameter that can be used instead of datamodel for telescope model constrution
* Removed the requirement to test the value of the UVW table in the tests

1.6.9
-----

* Added mswriter parameters 'timestamp_output', 'command_template' and 'max_payloads'
* Added receive loop with 'continuous_mode' parameter
* Added timestamped ms names

1.6.8
-----

* Bugfix for receiving data outside model timerange
* Hide ms opening log messages

1.6.7
-----

* Added ms-asserter entrypoint for testing pipeline outputs
* Added configurable ring buffer option to mswriter
* emu-send repeat option increments now timestamps
* emu-recv updated to allow receiving timestamps beyond the model range

1.6.6
-----

* Removed the copy from the Dockerfile. This will increase the size - but should fix
  the issues related to missing functionality in the image.

1.6.5
-----

* Small update to the default Docker image to include plasma

1.6.4
-----

* Added Dockerfiles for dockerised tests of emulator and receive workflows

1.6.3
-----

* changed the repo name again to place it in the sdp sub-group.
* Updated the dependencies on the DAL.

1.6.2
-----
* changed the repo name
* added the .release file


1.6.1
-----

* Version not released due to issues with publication on the CAR

1.6
---

* Added optional support for using plasmaStMan for the DATA column
  of the main table.
* Added option to execute commands
  for each newly created Measurement Set
  with ``plasma-mswriter``.
* Corrected spectral window values when writing measurement sets.
* Added a null_consumer that provides an example for developers. Functionally just drops the payloads
* Added new BDD tests and data for AA0.5LOW configurations
* Changed the functionality of ``reader.num_repeats`` - it now resends the number of timestamps set by
  ``reader.num_timestamps`` the stated number of times - each separated by ``transmission.time_interval``.

1.5
---

* Added new ``transmission.time_interval`` option
  to control the time to let pass between transmissions of data
  of successive timestamps.
  By default this is calculated
  from the ``TIME`` values of the input Measurement Set,
  but arbitrary, fixed intervals, as well as null intervals,
  can be specified.
* Removed our dependency on OSKAR to read and write Measurement Sets.
  The code now uses the python casacore bindings instead,
  leading to a simpler installation procedure,
  both for developers and users.
* Ported code to use new name/version of the SKA logging library.
* Fixed and simplified configuration requirements
  of few of the tools and modules,
  so running the system requires less values
  to be explicitly given in order to run.
* Added automatic publishing of python package to Nexus
  when tags are pushed.

1.4
---

* Ported the plasma caller and processor
  to use Arrow Tables to transport visibility metadata
  rather than binary tensors with pickled content.
  This makes it possible to invoke processors
  that do not use our data types,
  and that are potentially written in a different language.
* The schemas defining the Tables and procedures
  are imported from the ``ska-sdp-dal-schemas`` package.

1.3
---

* Fixed a problem with a test failing
  when the plasma extra was not installed.
* Pin sdp-dal-prototype dependency
  to avoid breaking changes upstream.
* Removed dependency on ``astropy``.
  This package adds ~50 [MB] of data to our installations,
  while in return we use a single bit of functionality
  that is easily implemented.
* Remove old, unnecessary dependency on ``sh`` package.

1.2
---

* Added ability to specify user-provided consumer classes,
  which will allow us to use third-party consumer code
  without us having to maintain it.
* Added ``unix_time`` property to ``Payload`` class,
  which is compatible with values from the ``time`` built-in module.
* Updated to to work with spead2 versions 2 and 3.
* Updated test infrastructure to work
  against latest version of pytest-bdd.

1.1
---

* Developed new reception consumer
  that writes incoming payload data
  into an Apache Plasma store
  using the `sdp-dal-prototype <https://gitlab.com/ska-telescope/sdp-dal-prototype>` code.
  In the sdp-dal-prototype parlance this is a "Caller".
* Developed a corresponding "Processor"
  that reads the data off the Plasma store
  and writes it into a Measurement Set.
* General bug fixes and improvements.


1.0.1
-----

* Minor adjustments
  to example data used by demonstrations.

1.0
---

* First public release.
* SPEAD-based transmission and reception of ICD payloads
  implemented.
* Receivers use a generic consumer architecture
  to handle incoming payloads offered.
* Specific consumer implemented
  that takes incoming payloads
  and write them into a Measurement Set.
* It is possible to transmit data for multiple channels
  through a single SPEAD stream.
* UDP multicast transmission works.
* Simple Helm charts demonstrating functionality
  are available as examples.

0.3
---

* Further fixes, still not fully operational

0.2
---

* First tag, still not fully operational
