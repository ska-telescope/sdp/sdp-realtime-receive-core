import functools
import logging
from dataclasses import dataclass, replace

import numpy as np

logger = logging.getLogger(__name__)


@dataclass(frozen=True)
class ChannelRange:
    """Describes a range of channels"""

    start_id: int
    """The first channel in this range"""

    count: int
    """The number of channels in this range"""

    stride: int = 1
    """The gap between each channel in this range"""

    def __post_init__(self):
        if self.start_id < 0:
            raise ValueError("start_id must be non-negative")
        if self.count < 1:
            raise ValueError("count must be positive")
        if self.stride < 1:
            raise ValueError("stride must be positive")

    @staticmethod
    def from_str(channel_range: str):
        """
        Construct a channel range from a string in the format
        ``start_id:count`` or ``start_id:count:stride``.
        If not specified, ``stride=1``.
        """
        parts = list(int(p) for p in channel_range.split(":"))
        if len(parts) < 2 or len(parts) > 3:
            raise ValueError(f"Invalid format: {channel_range}")

        return ChannelRange(*parts)

    @staticmethod
    def from_ndarray(arr: np.ndarray):
        """
        Construct a channel range that matches the channel ids in the specified
        array, raising a ValueError if unable to do so.
        """
        if not np.issubdtype(arr.dtype, np.integer):
            raise ValueError(
                f"Only able to create ChannelRange from integer data. Given {arr.dtype}"
            )

        if len(arr.shape) != 1:
            raise ValueError(
                f"Only able to create ChannelRange in one dimension. Given {arr.shape}"
            )

        if len(arr) == 1:
            channels = ChannelRange(arr[0], len(arr))
            logger.warning(
                "Unable to determine stride from a single channel. Defaulting to %d",
                channels.stride,
            )
            return channels

        diffs = np.diff(arr)
        uq = np.unique(diffs)

        if len(uq) != 1:
            raise ValueError(
                f"Unable to create ChannelRange with heterogeneous strides. Found strides of size {uq}"
            )

        return ChannelRange(arr[0], len(arr), uq.item())

    @functools.cached_property
    def end_id(self):
        """The last channel id in this range (inclusive)"""
        return self.start_id + self.stride * (self.count - 1)

    @property
    def _stop_id(self):
        """
        The first channel id past the end of the range that is a multiple
        of the stride
        """
        return self.start_id + self.stride * self.count

    def __str__(self) -> str:
        """This range, in slicing notation"""
        return f"{self.start_id}:{self._stop_id}:{self.stride}"

    def as_slice(self, indexed_from=0):
        """
        Convert this channel range to a slice that can be used to index into
        a vector.

        :param indexed_from: The channel_id for index 0
            e.g. indexed_from=3 means that index=0 is channel_id 3).
        """
        return slice(
            self.start_id - indexed_from,
            self._stop_id - indexed_from,
            self.stride,
        )

    def as_np_range(self, dtype=np.int64):
        """This range, represented as a ndarray of channel ids"""
        return np.arange(self.start_id, self._stop_id, self.stride, dtype=dtype)

    def try_create_subrange(
        self,
        start_id: int | None = None,
        max_channels: int | None = None,
        new_stride: int | None = None,
    ):
        """
        Attempt to create a subrange of this range with the specified
        constraints. If this isn't possible, return None.

        :param start_id: The first channel of the new range. If it doesn't
            lie on the existing range, then None is returned.
        :param max_channels: The maximum number of channels in the new range
        :param new_stride: The stride of the new range, should be an integer
            multiple of the current range, otherwise None is returned.
        """

        new_range = self

        if start_id is not None:
            if new_range.contains_channel(start_id):
                removed_channels = (start_id - new_range.start_id) // new_range.stride
                new_range = replace(
                    new_range,
                    start_id=start_id,
                    count=new_range.count - removed_channels,
                )
            else:
                return None

        if new_stride is not None:
            q, mod = divmod(new_stride, new_range.stride)
            if mod == 0:
                excess_count = new_range.count % q
                new_range = replace(
                    new_range,
                    stride=new_stride,
                    count=(new_range.count // q) + excess_count,
                )
            else:
                return None

        if max_channels is not None and new_range.count > max_channels:
            new_range = replace(new_range, count=max_channels)

        return new_range

    def contains_range(self, rhs: "ChannelRange"):
        """
        Returns true if the specified range lies in this range
        """
        return (
            self.contains_channel(rhs.start_id)
            and self.contains_channel(rhs.end_id)
            # Ensure that other range has an integer multiple stride
            and rhs.stride % self.stride == 0
        )

    def contains_channel(self, channel_id: int):
        """
        Returns true if the specified channel lies in this range
        """
        return (
            self.start_id <= channel_id <= self.end_id
            # Channel lies on a strided channel in this range
            and (self.stride == 1 or (channel_id - self.start_id) % self.stride == 0)
        )
