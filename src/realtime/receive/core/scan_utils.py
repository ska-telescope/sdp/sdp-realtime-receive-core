"""
Utility functions to create ScanType objects out of different JSON documents
used throughout SDP.
"""
from typing import Callable, List, Union

import numpy as np
from astropy import units as u
from astropy.coordinates import Angle, SkyCoord

from realtime.receive.core.common import from_dict, load_json_resource
from realtime.receive.core.scan import (
    Beam,
    Channels,
    Field,
    PhaseDirection,
    Polarisations,
    ScanType,
    SpectralWindow,
    StokesType,
)


def parse_scantypes_from_assignres(
    assign_resources_command: Union[dict, str],
) -> List[ScanType]:
    """Parses scan types from a subarray AssignRes Tango command argument"""
    if isinstance(assign_resources_command, str):
        assign_resources_command = load_json_resource(assign_resources_command)
    # use backwards compatibility if specified by interface
    interface = assign_resources_command.get("interface")
    if interface == "https://schema.skao.int/ska-sdp-assignres/0.4":
        return _parse_scantypes_from_execblock_04(assign_resources_command["execution_block"])
    elif interface == "https://schema.skao.int/ska-sdp-assignres/1.0":
        return _parse_scantypes_from_execblock_10(assign_resources_command["execution_block"])
    else:
        raise ValueError(f"Unsupported interface {interface}")


def parse_scantypes_from_execblock(execution_block: Union[dict, str]) -> List[ScanType]:
    """Parse scan types from an execution block (as a dict or raw json)"""
    if isinstance(execution_block, str):
        execution_block = load_json_resource(execution_block)
    # I'm not sure if EBs always declare a schema. If they don't, then we use
    # this (flaky) condition.
    sample_phase_dir = execution_block["fields"][0]["phase_dir"]
    if "ra" in sample_phase_dir or "ra_str" in sample_phase_dir:
        return _parse_scantypes_from_execblock_04(execution_block)
    elif "target_name" in sample_phase_dir and "reference_frame" in sample_phase_dir:
        return _parse_scantypes_from_execblock_10(execution_block)
    else:
        raise ValueError(f"Unsupported execution block {execution_block}")


def _parse_scantypes_from_execblock_04(execution_block: Union[dict, str]) -> List[ScanType]:
    """
    Parse the ScanType objects from the given SDP Execution Block following the
    ska-sdp-assignres/0.4 schema at:
    https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/ska-sdp-assignres.html#sdp-assign-resources-0-4

    :param execution_block: The Execution Block
    """
    return _parse_scantypes_from_execblock_04_or_10(execution_block, _extract_phase_direction_04)


def _parse_scantypes_from_execblock_10(execution_block: Union[dict, str]) -> List[ScanType]:
    """
    Parse the ScanType objects from the given SDP Execution Block following the
    ska-sdp-assignres/1.0 schema at:
    https://developer.skao.int/projects/ska-telmodel/en/latest/schemas/ska-sdp-assignres.html#sdp-assign-resources-1-0

    :param execution_block: The Execution Block
    """
    return _parse_scantypes_from_execblock_04_or_10(execution_block, _extract_phase_direction_10)


def _parse_scantypes_from_execblock_04_or_10(
    execution_block: dict, make_phase_direction: Callable[[dict], PhaseDirection]
) -> list[ScanType]:
    polarisations = {
        polarisations["polarisations_id"]: Polarisations(
            polarisations["polarisations_id"],
            [StokesType[corr_type] for corr_type in polarisations["corr_type"]],
        )
        for polarisations in execution_block["polarisations"]
    }

    fields = {
        field["field_id"]: Field(
            field["field_id"],
            make_phase_direction(field["phase_dir"]),
            pointing_fqdn=field.get("pointing_fqdn", None),
        )
        for field in execution_block["fields"]
    }

    channels = {
        channel["channels_id"]: Channels(
            channel["channels_id"],
            [
                from_dict(SpectralWindow, data=spectral_window)
                for spectral_window in channel["spectral_windows"]
            ],
        )
        for channel in execution_block["channels"]
    }

    def combine_beams(*beams: dict) -> Beam:
        """Overrides a partial beam"""
        beam = {}
        for beam_overwrite in beams:
            beam.update(beam_overwrite)
        return Beam(
            beam["beam_id"],
            beam["function"],
            channels[beam["channels_id"]],
            polarisations[beam["polarisations_id"]],
            fields[beam["field_id"]],
        )

    base_beams = {beam["beam_id"]: beam for beam in execution_block["beams"]}

    def get_base_beam(beam_id):
        """
        Return the raw, potentially partial beam definition in the
        AssignResources command, or an empty dictionary if no such definition
        exists.
        """
        return base_beams.get(beam_id, {})

    base_scan_type_beam_overrides = {
        scan_type["scan_type_id"]: scan_type["beams"]
        for scan_type in execution_block["scan_types"]
        if scan_type["scan_type_id"].startswith(".")
    }

    def get_base_scan_type_beam_override(scan_type, beam_id):
        if "derive_from" not in scan_type:
            return {}
        return base_scan_type_beam_overrides[scan_type["derive_from"]].get(beam_id, {})

    scan_types = [
        ScanType(
            scan_type["scan_type_id"],
            [
                combine_beams(
                    get_base_beam(beam_id),
                    get_base_scan_type_beam_override(scan_type, beam_id),
                    beam_override,
                )
                for beam_id, beam_override in scan_type["beams"].items()
            ],
        )
        for scan_type in execution_block["scan_types"]
        if not scan_type["scan_type_id"].startswith(".")
    ]
    return scan_types


def _extract_phase_direction_04(phase_direction: dict) -> PhaseDirection:
    return PhaseDirection(
        ra=_extract_coordinate_adr49(phase_direction, "ra"),
        dec=_extract_coordinate_adr49(phase_direction, "dec"),
        reference_time=phase_direction["reference_time"],
        reference_frame=phase_direction["reference_frame"],
    )


def _extract_phase_direction_10(phase_direction: dict) -> PhaseDirection:
    frame = phase_direction["reference_frame"]
    attrs = phase_direction["attrs"]
    if frame in ("icrs", "galactic"):
        coord = SkyCoord([attrs["c1"]], [attrs["c2"]], frame=frame, unit=u.deg)
        if frame == "galactic":
            coord = coord.transform_to("icrs")
    else:
        raise ValueError(f"Unsupported reference frame {frame}")
    return PhaseDirection(
        ra=coord.ra,
        dec=coord.dec,
        reference_time="J2000",
        reference_frame="icrs",
    )


def _extract_coordinate_adr49(phase_dir: dict, key: str) -> Angle:
    """
    Extracts coordinates according to SKA ADR-49 symbols, units
    and sexagesimal representation.
    """
    sg_units = u.hour if key in ("ra", "ha") else u.deg
    return (
        Angle(phase_dir[key], u.deg)
        if key in phase_dir
        else Angle(phase_dir[f"{key}_str"], sg_units)
    )


def _extract_coordinate_03(phase_dir: dict, key: str) -> Angle:
    """
    Deprecated coordinate extraction for ska-sdp-assignres/0.3
    """
    sg_units = u.hour if key in ("ra", "ha") else u.deg
    return Angle(np.array(phase_dir[key]).reshape(-1), sg_units)
