from typing import NamedTuple

import numpy as np


# We construct many of these in the pointing calibration
# pipeline and a non-trivial amount of the run time is
# spent constructing these objects. NamedTuple is much
# faster than @dataclass in this case, hence why we're
# using it here.
class Pointing(NamedTuple):
    """Represents a simplified version of a single row in a measurement set pointing table"""

    antenna_id: int
    """Antenna identifier"""
    time: float
    """Unix time for when this pointing was observed"""
    direction: tuple[np.double, np.double]
    """Antenna pointing direction in Az/Al"""
    target: tuple[np.double, np.double]
    """Target pointing direction in Az/Al"""
    pointing_offset: tuple[np.double, np.double] = (0.0, 0.0)
    """The offset in Az/Al that was applied to the target pointing direction"""
    source_offset: tuple[np.double, np.double] = (0.0, 0.0)
    """The difference between the target and direction pointings"""
    name: str = ""
    """Pointing direction name; user specified"""
    tracking: bool = True
    """True if tracking the nominal pointing position"""
