# -*- coding: utf-8 -*-
"""Configuration related utilities"""


def augment_config(config, options):
    """Apply key-value options to the given config"""
    for opt in options:
        name, value = opt.split("=")
        category, name = name.split(".")
        if category not in config:
            config[category] = {}
        config[category][name] = value
