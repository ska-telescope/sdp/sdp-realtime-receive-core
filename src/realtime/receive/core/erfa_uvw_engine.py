from typing import List, Optional, Sequence

import erfa
import numpy as np
from astropy.coordinates import Angle
from astropy.time import Time
from astropy.utils.iers import IERS_Auto

from .antenna import Antenna
from .uvw_engine import State, UVWEngine
from .uvw_utils import get_r, get_rot_x, get_rot_y, get_rot_z


class ErfaUVWEngine(UVWEngine):
    """
    A derived class that uses erfa matrices and transformations
    to determine the UVW of the antennas.

    There are a few stages:
    1. The antenna are transformed from ITRF to GCRS. The ITRF is a
    frame of reference that rotates w.r.t. the stars. The GCRS is a
    frame of reference that is fixed w.r.t. the stars. This transformation
    is performed via


    GCRF = P^T N^T Rs^T Rm^T ITRF

    P == Transformation matrix from reference EPOCH to current time
    N == Nutation matrix associated with the nutation at current time
    Rs == Transformation matrix for earth orientation around the CEP at current time
    Rm == Polar motion matrix for the current time

    The transformation matrices are calculated using the erfa matrices and is the same
    for all antennas and only has to be performed once per time step.

    Once the antennas are in GCRF, the UVW is calculated using the standard XYZ to UVW
    projection matrix. This is now in the catalog frame of reference. So the RA and Dec
    of the source are the catalogue values. The antenna UVW is the baseline from the centre of the
    Earth to the antenna in the direction of the source.

    The baselines can then be calculated by forming differences between the antennas.

    Note: the overladed methods are get_uvw, and _update_antenna_uvw. This is to provide access to the
    caching. The static get_antenna_uvw is not overloaded as it is not used in the engine.


    """

    def __init__(
        self,
        antennas: Sequence[Antenna],
        baselines: Optional[Sequence[tuple]] = None,
        swap_baselines=False,
        include_autos=True,
        lower_triangular=False,
        position_frame="ITRF",
        direction_frame="ICRS",
    ):
        """
        Initialise the engine

        :param Sequence[Antenna] antennas: The antennas to use
        :param Sequence[tuple] baselines: The baselines to use
        :param Bool swap_baselines: Whether to swap the baselines
        :param Bool include_autos: Whether to include the autos
        :param Bool lower_triangular: Whether to include the lower triangular baselines
        :param str position_frame: The frame of the input positions (currently assumes geocentric itrf)
        :param str direction_frame: The frame of the input directions (currently assumes ICRS)
        """
        super().__init__(
            antennas,
            baselines,
            swap_baselines,
            include_autos,
            lower_triangular,
            position_frame,
            direction_frame,
        )

        # set up the ITRF to GCRF matrix
        self.itrf2gcrf = None
        self.xyz2uvw = None
        self.precession = None
        self.nutation = None
        self.rm = None
        self.rs = None

        # set up Time
        self.epoch_of_date = None
        # set up the IERS
        self.iers = IERS_Auto().open()

    def set_xyz2uvw_matrix(self, source_ra: Angle, source_dec: Angle):
        """
        Simple metod to calcuate the xyz2uvw matrix from the source RA and Dec

        This assumes the rotation to the catalogue frame has already occurred

        :param astropy.Angle source_ra: The source RA
        :param astropy.Angle source_dec: The source Dec

        """
        h = -1 * source_ra
        d = source_dec

        self.xyz2uvw = get_r(h, d)

    def set_transformations(
        self,
        epoch: Time,
        source_ra: Angle,
        source_dec: Angle,
        correct_aberration=False,
    ):
        """
        A method to set the ITRF to GCRF transformation matrix

        GCRF = P^T N^T Rs^T Rm^T ITRF
            P == Transformation matrix from reference EPOCH to current time
            N == Nutation matrix associated with the nutation at current time
            Rs == Transformation matrix for earth orientation around the CEP at current time
            Rm == Polar motion matrix for the current time

            Rm == R2(-X)R1(-Y) where X and Y are the angles of the CElestial Intermediate Pole
            given by iers bulletin A (see erfa docs) - R1 is a 3d rotation about the x axis R2 the
            y axis and R3 the z axis. The angles are in radians.

            We can get it from the iers module astropy.utils.iers.dcip_xy(jd1, jd2)

            Rs == R3(ERA) where ERA == Earth Rotation Angle at the epoch

        :param astropy.Time epoch: The time at which the transformation is to be performed
        """
        # get the transformation matrix from ITRF to GCRS
        # this is the same for all antennas
        # the erfa function wants a julian date
        # the astropy time object has a jd attribute
        # pylint: disable=no-member

        if self.epoch_of_date is None or self.epoch_of_date != epoch:
            model = erfa.pn06a(epoch.jd1, epoch.jd2)
            self.precession = np.array(model[5]).squeeze()
            self.nutation = np.array(model[6]).squeeze()
            x, y = self.iers.dcip_xy(epoch.jd1, epoch.jd2)

            self.rm = get_rot_y(Angle(-1 * x, unit="rad")) @ get_rot_x(Angle(-1 * y, unit="rad"))
            era = Angle(epoch.sidereal_time("apparent", "greenwich"))
            # era = Angle(erfa.era00(epoch.jd1, epoch.jd2), unit="rad")
            self.rs = get_rot_z(era)

            self.itrf2gcrf = self.precession.T @ self.nutation.T @ self.rs.T @ self.rm.T
            self.itrf2gcrf = self.itrf2gcrf.squeeze()

            # TODO: Maybe correct RA and Dec for aberration you would do this
            # By obtaining the velocity of the earth and then using the erfa.ab function
            # to get the new direction - then converting that back to RA and Dec (ICRS).

            if correct_aberration:
                # I've tried to do this naively and it did not work. Will generate a
                # a new ticket to deal with this
                raise NotImplementedError

            self.set_xyz2uvw_matrix(source_ra, source_dec)

            self.epoch_of_date = epoch

    def _update_antenna_uvws(self, requested_state: State) -> List[np.ndarray]:
        """
        Internal class to manage the updating of the UVW of the antenna. This is essentially the
        baseline between the antenna location and the centre of the earth.

        """
        # get the transformation matrix from ITRF to GCRS
        epoch = requested_state.time
        target = requested_state.phase_direction
        ra = target.ra
        dec = target.dec

        self.set_transformations(epoch, ra, dec)
        uvw_list: List[np.ndarray] = []
        for index in range(self.num_stations()):
            antenna = self._antennas[index]

            pos = np.array([antenna.x, antenna.y, antenna.z])

            xyz = self.itrf2gcrf.dot(pos)

            # xyz are now in a GCRF frame - essentially the baselines are
            # running from the geocentre to the antenna.

            uvw = self.xyz2uvw.dot(xyz)

            uvw_list.append(uvw)

        return uvw_list

    @staticmethod
    def get_antenna_uvw(
        antenna_location: np.ndarray,
        epoch: Time,
        ra: Angle,
        dec: Angle,
        position_frame="itrf",
        epoch_frame="J2000",
        swap_baselines=False,
    ) -> np.ndarray:
        raise NotImplementedError
