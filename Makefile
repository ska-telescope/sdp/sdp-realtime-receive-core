# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

include .make/base.mk
include .make/python.mk

PYTHON_LINE_LENGTH = 99
DOCS_SPHINXOPTS = -W --keep-going
PYTHON_SWITCHES_FOR_FLAKE8 = --select E --ignore=E501,W503,E731
CASACORE_MEASURES_DIR ?= /usr/local/share/casacore/data
MEASUREMENT_SETS_FOR_TESTS = sim-vis.ms.tar.gz AA05LOW.ms.tar.gz

python-pre-build:
	pip install build

python-pre-test:
	./install_measures.sh "$(CASACORE_MEASURES_DIR)"
	./extract_data.sh $(MEASUREMENT_SETS_FOR_TESTS)

docs-pre-build:
	poetry config virtualenvs.create $(POETRY_CONFIG_VIRTUALENVS_CREATE)
	poetry install --only main,docs

docs-serve:
	sphinx-autobuild docs/src docs/build

python-benchmark: PYTHON_VARS_AFTER_PYTEST = --benchmark-only --benchmark-autosave
python-benchmark: python-test
