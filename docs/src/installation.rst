Installation
============

This is a standard setuptools-based program
so the usual installation methods should work,
and all dependencies should be automatically pulled.
The only caveat is that some of the dependencies
are found in the CAR server instead of PyPI,
so we need to point ``pip`` to the CAR in order to them::

  # Go into the top-level directory of this repository
  cd ska-sdp-realtime-receive-core

  # Using "pip" will automatically install any dependencies from PyPI
  pip install --extra-index-url=https://artefact.skao.int/repository/pypi-all/simple .

  # Use pip in editable mode if you are actively changing the code
  pip install --extra-index-url=https://artefact.skao.int/repository/pypi-all/simple -e .

Note that this package can also be installed directly via pip
from the CAR itself::

 pip install --extra-index-url=https://artefact.skao.int/repository/pypi-all/simple \
   ska-sdp-realtime-receive-core

Alternatively, if you are developing this package, you may want to install using poetry::

 poetry install

.. note::
  If installing on ARM-based Mac, you get the following error:
  ``Unable to find installation candidates for python-casacore``

  If you encounter this, enter the virtual environment with ``poetry shell`` and run
  the following command to properly install python-casacore::

    CFLAGS="-I/opt/homebrew/include/" LDFLAGS="-L/opt/homebrew/lib/" LD_LIBRARY_PATH=/opt/homebrew/lib pip install python-casacore

  If you are running VSCode on MacOS ARM64, make sure you are running the universal version.
  Verify this by right-clicking on VSCode inside Applications, and selecting Get Info.
  In the terminal, run ``arch`` and verify the output is ``arm64``.
