SDP Realtime Receive Core
===========================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sdp-realtime-receive-core/badge/?version=latest)](https://developer.skao.int/projects/ska-sdp-realtime-receive-core/en/latest/?badge=latest)

This is the python package of simple utilities and interfaces used by the realtime receive modules and the cbf emulator

## Installation

Installation instructions can be found in the online [documentation](https://developer.skao.int/projects/ska-sdp-realtime-receive-core/en/latest/?badge=latest).
