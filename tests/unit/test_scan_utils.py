import os

import numpy as np
import pytest

from realtime.receive.core.scan_utils import parse_scantypes_from_assignres

RTOL = 1e-15


@pytest.mark.parametrize(
    "sched_file",
    ["tests/data/assign_resources_0_4.json", "tests/data/assign_resources_1_0.json"],
)
def test_schedtm(sched_file: str):
    scan_types = parse_scantypes_from_assignres(sched_file)
    scan_type = scan_types[0]
    assert 4 == scan_type.num_channels
    sw = scan_type.beams[0].channels.spectral_windows[0]
    assert 4 == sw.count
    assert 0.35e9 == sw.freq_min
    assert 0.368e9 == sw.freq_max
    expected_bw = (0.368e9 - 0.35e9) / 7
    assert expected_bw == sw.channel_bandwidth
    # Stride of 2 effectively halves the bandwidth/doubles the distance between channels
    assert expected_bw * 2 == sw.channel_width
    assert 4 == scan_type.num_pols
    np.testing.assert_allclose(
        np.array([[0.709823297846581], [-0.00023193486504280203]]),
        np.array(
            [
                scan_type.beams[0].field.phase_dir.ra.rad,
                scan_type.beams[0].field.phase_dir.dec.rad,
            ]
        ),
        rtol=RTOL,
    )
