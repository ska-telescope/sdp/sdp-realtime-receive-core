import numpy as np
import pytest
from numpy.testing import assert_array_equal

from realtime.receive.core import ChannelRange


def test_invalid_values_raise_value_error():
    with pytest.raises(ValueError):
        ChannelRange(start_id=-1, count=1)
    with pytest.raises(ValueError):
        ChannelRange(start_id=0, count=0)
    with pytest.raises(ValueError):
        ChannelRange(start_id=0, count=1, stride=0)


@pytest.mark.parametrize(
    "value, expected",
    [
        ("0:10", ChannelRange(0, 10)),
        ("5:20", ChannelRange(5, 20)),
        ("12:20:3", ChannelRange(12, 20, 3)),
        ("", ValueError),
        ("5", ValueError),
        ("abc", ValueError),
        ("abc:def", ValueError),
        ("5:10:3:4", ValueError),
    ],
)
def test_from_str(value: str, expected: ChannelRange | type[ValueError]):
    if isinstance(expected, ChannelRange):
        assert ChannelRange.from_str(value) == expected
    else:
        with pytest.raises(expected):
            ChannelRange.from_str(value)


@pytest.mark.parametrize(
    "values, expected_stride",
    [
        ([1, 2, 3, 4, 5], 1),
        ([5, 6, 7, 8, 9], 1),
        ([3, 5, 7, 9, 11], 2),
        ([5, 10, 15, 20, 25], 5),
        ([4], ChannelRange.stride),
    ],
)
def test_from_ndarray(values: list[int], expected_stride: int):
    channels = ChannelRange.from_ndarray(np.array(values))
    assert channels.start_id == values[0]
    assert channels.count == len(values)
    assert channels.stride == expected_stride


@pytest.mark.parametrize(
    "values",
    [
        [1.0, 2.0, 3.0],
        [[1, 2], [3, 4]],
        [1, 2, 4],
        [3, 2, 1],
    ],
)
def test_from_ndarray_with_invalid_values(values: list[int]):
    with pytest.raises(ValueError):
        ChannelRange.from_ndarray(np.array(values))


@pytest.mark.parametrize(
    "start_id, max_channels, new_stride, expected_channels",
    [
        (None, None, None, ChannelRange(100, 10, 2)),
        (None, None, 4, ChannelRange(100, 5, 4)),
        (None, 8, None, ChannelRange(100, 8, 2)),
        (None, 8, 4, ChannelRange(100, 5, 4)),
        (102, None, None, ChannelRange(102, 9, 2)),
        (102, None, 4, ChannelRange(102, 5, 4)),
        (102, 8, None, ChannelRange(102, 8, 2)),
        (102, 2, 4, ChannelRange(102, 2, 4)),
    ],
)
def test_try_create_subrange_with_valid_values(
    start_id: int | None,
    max_channels: int | None,
    new_stride: int | None,
    expected_channels: ChannelRange,
):
    ch = ChannelRange(100, 10, 2)
    ch = ch.try_create_subrange(start_id, max_channels, new_stride)
    assert ch is not None
    assert (
        expected_channels == ch
    ), f"Expected {expected_channels.as_np_range()}, got {ch.as_np_range()}"


def test_try_create_subrange_edge_cases():
    subrange = ChannelRange(0, 4).try_create_subrange(1, 2, 2)
    assert ChannelRange(1, 2, 2) == subrange

    subrange = ChannelRange(0, 4).try_create_subrange(3, 1, 1)
    assert ChannelRange(3, 1, 1) == subrange


@pytest.mark.parametrize("start_id", [99, 101, 102, 130, 133])
def test_try_create_subrange_with_invalid_start_id(start_id: int):
    ch = ChannelRange(100, 10, 3)
    assert ch.try_create_subrange(start_id=start_id) is None


@pytest.mark.parametrize(
    "channels, end_id, stop_id",
    [
        (ChannelRange(0, 5), 4, 5),
        (ChannelRange(1, 5), 5, 6),
        (ChannelRange(0, 10, 2), 18, 20),
        (ChannelRange(1, 10, 3), 28, 31),
    ],
)
def test_helper_properties_give_correct_values(
    channels: ChannelRange,
    end_id: int,
    stop_id: int,
):
    assert channels.end_id == end_id
    # pylint: disable-next=protected-access
    assert channels._stop_id == stop_id


@pytest.mark.parametrize(
    "channels, indexed_from, expected_slice",
    [
        (ChannelRange(0, 10), None, slice(0, 10, 1)),
        (ChannelRange(10, 10), 0, slice(10, 20, 1)),
        (ChannelRange(0, 10, 2), None, slice(0, 20, 2)),
        (ChannelRange(10, 10, 2), None, slice(10, 30, 2)),
        (ChannelRange(10, 10, 2), 10, slice(0, 20, 2)),
        (ChannelRange(10, 10, 2), 5, slice(5, 25, 2)),
    ],
)
def test_as_slice(
    channels: ChannelRange,
    indexed_from: int | None,
    expected_slice: slice,
):
    sl = channels.as_slice() if indexed_from is None else channels.as_slice(indexed_from)
    assert sl == expected_slice


@pytest.mark.parametrize(
    "channels, expected",
    [
        (ChannelRange(0, 10), np.arange(0, 10, 1)),
        (ChannelRange(10, 10), np.arange(10, 20, 1)),
        (ChannelRange(0, 10, 2), np.arange(0, 20, 2)),
        (ChannelRange(10, 10, 2), np.arange(10, 30, 2)),
    ],
)
def test_as_np_range(channels: ChannelRange, expected: np.ndarray):
    assert_array_equal(expected, channels.as_np_range())


@pytest.mark.parametrize(
    "channels, ch, contains",
    [
        (ChannelRange(100, 10), 99, False),
        (ChannelRange(100, 10), 100, True),
        (ChannelRange(100, 10), 101, True),
        (ChannelRange(100, 10), 102, True),
        (ChannelRange(100, 10), 108, True),
        (ChannelRange(100, 10), 109, True),
        (ChannelRange(100, 10), 110, False),
        (ChannelRange(100, 10, 2), 99, False),
        (ChannelRange(100, 10, 2), 100, True),
        (ChannelRange(100, 10, 2), 101, False),
        (ChannelRange(100, 10, 2), 102, True),
        (ChannelRange(100, 10, 2), 118, True),
        (ChannelRange(100, 10, 2), 119, False),
        (ChannelRange(100, 10, 2), 120, False),
    ],
)
def test_contains_channel(channels: ChannelRange, ch: int, contains: bool):
    assert channels.contains_channel(ch) == contains


@pytest.mark.parametrize(
    "lhs, rhs, contains_range",
    [
        (ChannelRange(100, 10), ChannelRange(100, 10), True),
        (ChannelRange(100, 10), ChannelRange(104, 4), True),
        (ChannelRange(100, 10), ChannelRange(101, 10), False),
        (ChannelRange(100, 10), ChannelRange(100, 5, 2), True),
        (ChannelRange(100, 10), ChannelRange(101, 5, 2), True),
        (ChannelRange(110, 10), ChannelRange(108, 10), False),
        (ChannelRange(100, 10, 2), ChannelRange(102, 5, 2), True),
        (ChannelRange(100, 10, 2), ChannelRange(101, 10, 2), False),
        (ChannelRange(100, 10, 2), ChannelRange(101, 5, 2), False),
    ],
)
def test_contains_range(lhs: ChannelRange, rhs: ChannelRange, contains_range: bool):
    assert lhs.contains_range(rhs) == contains_range
