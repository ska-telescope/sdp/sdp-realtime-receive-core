import pytest
from astropy.time import Time

from realtime.receive.core.time_utils import (
    mjd_to_icd,
    mjd_to_unix,
    skao_to_mjd,
    unix_to_icd,
    unix_to_mjd,
)

SECS_PER_DAY = 86400.0
SKAO_EPOCH = Time("2000-01-01", format="isot", scale="tai")


def test_mjd_to_unix_epoch():
    """Tests MJD in seconds conversions with unix timestamp"""
    UNIX_EPOCH_AS_MJD = 40587.0
    UNIX_EPOCH_AS_MJDS = UNIX_EPOCH_AS_MJD * SECS_PER_DAY

    # Test astropy epoch conversion is exact
    assert 0.0 == Time(UNIX_EPOCH_AS_MJD, format="mjd").unix
    assert 0.0 == mjd_to_unix(UNIX_EPOCH_AS_MJDS)


@pytest.mark.parametrize("abs_tolerance", [1e-6])
@pytest.mark.parametrize(
    "mjd_timestamp,utc_timestamp,tai_timestamp",
    [
        (40587.0, "1970-01-01T00:00:00.000", "1970-01-01T00:00:08.000"),
        (51543.9996296, "1999-12-31T23:59:27.997", "1999-12-31T23:59:59.997"),
    ],
)
def test_mjd_to_unix(
    mjd_timestamp: float, utc_timestamp: str, tai_timestamp: str, abs_tolerance: float
):
    """Tests MJD in seconds conversions with unix timestamp"""
    astropy_timestamp = Time(mjd_timestamp, format="mjd")
    mjds_timestamp = mjd_timestamp * SECS_PER_DAY

    # test conversion is reversable
    assert mjds_timestamp == unix_to_mjd(mjd_to_unix(mjds_timestamp))

    # test precision with astropy in seconds
    assert pytest.approx(astropy_timestamp.unix, abs=abs_tolerance) == mjd_to_unix(mjds_timestamp)

    # confirm recorded astropy UTC and TAI string format
    assert astropy_timestamp.utc.isot == utc_timestamp
    assert astropy_timestamp.tai.isot == tai_timestamp


def test_mjd_to_icd_epoch():
    """Tests MJD timestamp conversions with SKAO timestamp"""
    SKAO_EPOCH_AS_MJDS = unix_to_mjd(SKAO_EPOCH.unix)

    assert 0.0 == unix_to_icd(mjd_to_unix(SKAO_EPOCH_AS_MJDS))
    assert 0.0 == mjd_to_icd(SKAO_EPOCH_AS_MJDS)


@pytest.mark.parametrize("abs_tolerance", [1e-6])
@pytest.mark.parametrize(
    "mjd_timestamp,utc_timestamp,tai_timestamp",
    [
        (40587.0, "1970-01-01T00:00:00.000", "1970-01-01T00:00:08.000"),
        (51543.9996296, "1999-12-31T23:59:27.997", "1999-12-31T23:59:59.997"),
    ],
)
def test_mjd_to_icd(
    mjd_timestamp: float, utc_timestamp: str, tai_timestamp: str, abs_tolerance: float
):
    """Tests MJD timestamp conversions with SKAO timestamp"""
    astropy_timestamp = Time(mjd_timestamp, format="mjd")
    mjds_timestamp = mjd_timestamp * SECS_PER_DAY

    # test conversion is reversable
    assert mjds_timestamp == skao_to_mjd(mjd_to_icd(mjds_timestamp))

    # test precision with astropy in seconds
    assert pytest.approx(astropy_timestamp.unix, abs=abs_tolerance) == mjd_to_unix(mjds_timestamp)

    # confirm recorded astropy UTC and TAI string
    assert astropy_timestamp.utc.isot == utc_timestamp
    assert astropy_timestamp.tai.isot == tai_timestamp
