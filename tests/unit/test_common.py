from dataclasses import dataclass

from realtime.receive.core.common import autocast_fields

# pylint: disable=missing-class-docstring


def test_autocast_fields():
    # A simple class that we wrap in dataclass and autocast_fields
    class T:
        a: int
        b: float
        c: bool
        d: bool

    T = dataclass(autocast_fields(T))

    # The same, but not using annotations and in the reverse order
    @autocast_fields
    @dataclass
    class U:
        a: int
        b: float
        c: bool
        d: bool

    # The real test
    for cls in (T, U):
        value = cls("1", "0.3", "True", "False")
        assert value.a == 1
        assert value.b == 0.3
        assert value.c
        assert not value.d
